reload("user.options")
reload("user.plugins")
reload("user.keymaps")
reload("user.lsp")

-- Hack to get rid of stupid warning about bad buffers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.offsetEncoding = { "utf-16" }
require("lspconfig").clangd.setup({ capabilities = capabilities })
