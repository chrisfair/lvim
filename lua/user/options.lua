if vim.bo.filetype == 'rust' then
  vim.opt.shiftwidth = 2
  vim.opt.tabstop = 2
  vim.opt.expandtab = true
end

if vim.bo.filetype == 'c' then
  vim.opt.shiftwidth = 3
  vim.opt.tabstop = 3
  vim.opt.expandtab = true
end


if vim.bo.filetype == 'go' then
  vim.opt.shiftwidth = 4
  vim.opt.tabstop = 4
  vim.opt.expandtab = false
end

if vim.bo.filetype == 'python' then
  vim.opt.shiftwidth = 4
  vim.opt.tabstop = 4
  vim.opt.expandtab = true
end

vim.opt.relativenumber = true

-- Cursor options
vim.opt.cursorline = true
vim.opt.cursorcolumn = true
vim.opt.colorcolumn = 80

-- appearance options
lvim.transparent_window = true
lvim.colorscheme="tokyonight-night"
lvim.leader = "space"
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"


lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = false
lvim.builtin.dap.active = true


lvim.builtin.treesitter.auto_install = true

lvim.format_on_save = {
  pattern = {"*.lua", "*.go", "*.py","*.c", "*.rs", "*.cs"}
}

-- encoding options
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.offsetEncoding = { "utf-16"}
require("lspconfig").clangd.setup({ capabilities = capabilities})

