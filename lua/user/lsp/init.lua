require "user.lsp.languages.go"
require "user.lsp.languages.python"
require "user.lsp.languages.llvm"
require "user.lsp.languages.lldb"
require "user.lsp.languages.rust"
require "user.lsp.languages.js-ts"
require "user.lsp.languages.sh"

lvim.lsp.diagnostics.virtual_text = false

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {"java", "python", "cpp",}

vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "jdtls" })

local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  { command = "google_java_format", filetypes = { "java" } },
  { command = "stylua", filetypes = { "lua" } },
  { command = "shfmt", filetypes = { "sh", "zsh" } },
  { command = "prettier", filetypes = { "css" } },
}

formatters.setup {
  {
    command = "clang-format",
    filetype = { "c", "cpp", "cs", "java"},
    extra_args ={ " --style", "{IndentWidth: 4}" },
  }
}


local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.offsetEncoding = { "utf-16" }
require('lspconfig').clangd.setup({capabilities = capabilities})
