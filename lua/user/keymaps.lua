lvim.leader = "space"
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"

lvim.keys.normal_mode["<F6>"] = ":SymbolsOutline<CR>"
lvim.keys.normal_mode["<F5>"] = ":NvimTreeToggle<CR>"


